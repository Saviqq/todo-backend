package sk.saviq.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.FetchType.LAZY;
import static javax.persistence.GenerationType.IDENTITY;

/**
 * Java representation of Entity User stored in database table.
 * One of the main objects representing business logic of application.
 *
 * @author psavka
 */
@Entity
@Table(name = "user")
public class User {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "email")
    private String email;

    @OneToMany(fetch = LAZY, cascade = ALL)
    @JoinColumn(name = "user_id")
    private List<DailyTodo> dailyTodoList;

    @OneToMany(fetch = LAZY, cascade = ALL)
    @JoinColumn(name = "user_id")
    private List<WeeklyTodo> weeklyTodoList;

    public User() {
    }

    public User(String firstName, String lastName, String email) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<DailyTodo> getDailyTodoList() {
        return dailyTodoList;
    }

    public void setDailyTodoList(List<DailyTodo> dailyTodos) {
        this.dailyTodoList = dailyTodos;
    }

    public List<WeeklyTodo> getWeeklyTodoList() {
        return weeklyTodoList;
    }

    public void setWeeklyTodoList(List<WeeklyTodo> weeklyTodoList) {
        this.weeklyTodoList = weeklyTodoList;
    }

    public void addDailyTodo(DailyTodo dailyTodo) {
        if(dailyTodoList == null) {
            dailyTodoList = new ArrayList<DailyTodo>();
        }

        dailyTodoList.add(dailyTodo);
    }

    public void addWeeklyTodo(WeeklyTodo weeklyTodo){
        if(weeklyTodoList == null) {
            weeklyTodoList = new ArrayList<WeeklyTodo>();
        }

        weeklyTodoList.add(weeklyTodo);
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", dailyTodoList=" + dailyTodoList +
                ", weeklyTodoList=" + weeklyTodoList +
                '}';
    }
}
