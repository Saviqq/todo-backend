package sk.saviq.model;

import javax.persistence.*;
import java.util.Date;

import static javax.persistence.GenerationType.IDENTITY;
import static javax.persistence.TemporalType.DATE;

/**
 * Java representation of Entity WeeklyTodo stored in database table.
 * One of the main objects representing business logic of application.
 *
 * @author psavka
 */
@Entity
@Table(name = "weekly_todo")
public class WeeklyTodo {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "todo")
    private String todo;

    @Column(name = "done")
    private boolean done;

    @Column(name = "date")
    @Temporal(DATE)
    private Date date;

    public WeeklyTodo() {}

    public WeeklyTodo(String todo, boolean done, Date date) {
        this.todo = todo;
        this.done = done;
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTodo() {
        return todo;
    }

    public void setTodo(String todo) {
        this.todo = todo;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "WeeklyTodo{" +
                "id=" + id +
                ", todo='" + todo + '\'' +
                ", done=" + done +
                ", date=" + date +
                '}';
    }
}
