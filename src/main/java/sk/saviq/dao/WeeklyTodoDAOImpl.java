package sk.saviq.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import sk.saviq.exception.WeeklyTodoNotFoundException;
import sk.saviq.model.User;
import sk.saviq.model.WeeklyTodo;

import javax.persistence.TypedQuery;
import java.util.Date;
import java.util.List;

/**
 * Data access object implementation of {@link WeeklyTodoDAO} for {@link WeeklyTodo}.
 *
 * @author psavka
 */
@Repository
public class WeeklyTodoDAOImpl implements WeeklyTodoDAO {

    private SessionFactory sessionFactory;

    @Autowired
    public WeeklyTodoDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    /**
     * {@inheritDoc}
     */
    public WeeklyTodo getWeeklyTodoById(int weeklyTodoId) {
        Session session = sessionFactory.getCurrentSession();

        WeeklyTodo weeklyTodo = session.get(WeeklyTodo.class, weeklyTodoId);
        if (weeklyTodo == null) {
            throw new WeeklyTodoNotFoundException("WeeklyTodo with id {" + weeklyTodoId +"} not found");
        }

        return weeklyTodo;
    }

    /**
     * {@inheritDoc}
     */
    public List<WeeklyTodo> getAllWeeklyTodoOfUser(int userId) {
        Session session = sessionFactory.getCurrentSession();

        TypedQuery<WeeklyTodo> query = session.createQuery("select D from WeeklyTodo D where user_id=:userId", WeeklyTodo.class);
        query.setParameter("userId", userId);

        List<WeeklyTodo> weeklyTodoList = query.getResultList();
        if(weeklyTodoList == null) {
            throw new WeeklyTodoNotFoundException("List of WeeklyTodo for User with id {" + userId +"} not found");
        }

        return weeklyTodoList;
    }

    /**
     * {@inheritDoc}
     */
    public List<WeeklyTodo> getAllWeeklyTodoOfUserForSpecificday(int userId, Date date) {
        Session session = sessionFactory.getCurrentSession();

        TypedQuery<WeeklyTodo> query = session.createQuery("select D from WeeklyTodo D where user_id=:userId and date=:date", WeeklyTodo.class);
        query.setParameter("userId", userId);
        query.setParameter("date", date);

        List<WeeklyTodo> weeklyTodoList = query.getResultList();
        if(weeklyTodoList == null) {
            throw new WeeklyTodoNotFoundException("List of WeeklyTodo for User with id {" + userId +"} and date {" + date + "} not found");
        }

        return weeklyTodoList;
    }

    /**
     * {@inheritDoc}
     */
    public void saveWeeklyTodoOfUser(WeeklyTodo weeklyTodo, int userId) {
        Session session = sessionFactory.getCurrentSession();

        User user = session.get(User.class, userId);
        if(user == null) {
            throw new WeeklyTodoNotFoundException("User with id {" + userId +"} not found, so WeeklyTodo was not saved");
        }
        user.addWeeklyTodo(weeklyTodo);

        session.save(user);
    }

    /**
     * {@inheritDoc}
     */
    public void updateWeeklyTodo(WeeklyTodo weeklyTodo) {
        Session session = sessionFactory.getCurrentSession();
        // TODO correct exception handling
        session.update(weeklyTodo);
    }

    /**
     * {@inheritDoc}
     */
    public void deleteWeeklyTodo(int weeklyTodoId) {
        Session session = sessionFactory.getCurrentSession();

        WeeklyTodo  weeklyTodo = session.get(WeeklyTodo.class, weeklyTodoId);
        if (weeklyTodo == null) {
            throw new WeeklyTodoNotFoundException("WeeklyTodo with id {" + weeklyTodoId +"} not found");
        }

        session.delete(weeklyTodo);
    }
}
