package sk.saviq.dao;

import sk.saviq.model.User;
import sk.saviq.model.WeeklyTodo;

import java.util.Date;
import java.util.List;

/**
 * Data Access Object interface for {@link WeeklyTodo}.
 * It defines methods that represents basic CRUD operations and
 * additional operations considering business logic of application.
 * <p>
 * Methods are implemented by {@link WeeklyTodoDAOImpl}.
 *
 * @author psavka
 */
public interface WeeklyTodoDAO {

    /**
     * Gets {@link WeeklyTodo} from database source using it's {@param weeklyTodoId }.
     *
     * @param weeklyTodoId - integer value representing Id of {@link WeeklyTodo}.
     * @return {@link WeeklyTodo}
     */
    WeeklyTodo getWeeklyTodoById(int weeklyTodoId);

    /**
     * Gets {@link List<WeeklyTodo>} from database source that belongs to {@link User} with {@param userId}.
     *
     * @param userId - integer value representing Id of {@link User}.
     * @return {@link List<WeeklyTodo>} - that belongs to the {@link User}.
     */
    List<WeeklyTodo> getAllWeeklyTodoOfUser(int userId);

    /**
     * Gets {@link List<WeeklyTodo>} from database source that belongs to {@link User} with {@param userId}
     * and are assigned to specific day given from the {@param date}
     *
     * @param userId - integer value representing Id of {@link User}
     * @param date   - date value by which {@link List<WeeklyTodo>} of {@link User} are assigned
     * @return {@link List<WeeklyTodo>} - that belongs to the {@link User} with specific day assigned
     */
    List<WeeklyTodo> getAllWeeklyTodoOfUserForSpecificday(int userId, Date date);

    /**
     * Saves new {@param weeklyTodo} of {@link WeeklyTodo} for {@link User} with {@param userId} to
     * database source.
     *
     * @param weeklyTodo - {@link WeeklyTodo} to be saved or updated.
     * @param userId     - integer value representing Id of {@link User}.
     */
    void saveWeeklyTodoOfUser(WeeklyTodo weeklyTodo, int userId);

    /**
     * Updates {@param weeklyTodo} of {@link WeeklyTodo} with new data in database source
     *
     * @param weeklyTodo- {@link WeeklyTodo} to be updated
     */
    void updateWeeklyTodo(WeeklyTodo weeklyTodo);

    /**
     * Deletes {@link WeeklyTodo} from database source with {@param weeklyTodoId}.
     *
     * @param weeklyTodoId - integer value representing Id of {@link WeeklyTodo} to be deleted.
     */
    void deleteWeeklyTodo(int weeklyTodoId);
}
