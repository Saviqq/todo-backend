package sk.saviq.dao;

import sk.saviq.model.DailyTodo;
import sk.saviq.model.User;

import java.util.Date;
import java.util.List;

/**
 * Data Access Object interface for {@link DailyTodo}.
 * It defines methods that represents basic CRUD operations and
 * additional operations considering business logic of application.
 * <p>
 * Methods are implemented by {@link DailyTodoDAOImpl}.
 *
 * @author psavka
 */
public interface DailyTodoDAO {

    /**
     * Gets {@link DailyTodo} from database source using it's {@param dailyTodoId }.
     *
     * @param dailyTodoId - integer value representing Id of {@link DailyTodo}.
     * @return {@link DailyTodo}
     */
    DailyTodo getDailyTodoById(int dailyTodoId);

    /**
     * Gets {@link List<DailyTodo>} from database source that belongs to {@link User} with {@param userId}.
     *
     * @param userId - integer value representing Id of {@link User}.
     * @return {@link List<DailyTodo>} - that belongs to the {@link User}.
     */
    List<DailyTodo> getAllDailyTodoOfUser(int userId);

    /**
     * Gets {@link List<DailyTodo>} from database source that belongs to {@link User} with {@param userId}
     * and are assigned to specific day given from the {@param date}
     *
     * @param userId - integer value representing Id of {@link User}
     * @param date   - date value by which {@link List<DailyTodo>} of {@link User} are assigned
     * @return {@link List<DailyTodo>} - that belongs to the {@link User} with specific day assigned
     */
    List<DailyTodo> getAllDailyTodoOfUserForSpecificDay(int userId, Date date);

    /**
     * Saves new {@param dailyTodo} of {@link DailyTodo} for {@link User} with {@param userId} to
     * database source.
     *
     * @param dailyTodo - {@link DailyTodo} to be saved or updated.
     * @param userId    - integer value representing Id of {@link User}.
     */
    void saveDailyTodoOfUser(DailyTodo dailyTodo, int userId);

    /**
     * Updates {@param dailyTodo} of {@link DailyTodo} with new data in database source
     *
     * @param dailyTodo- {@link DailyTodo} to be updated
     */
    void updateDailyTodo(DailyTodo dailyTodo);


    /**
     * Deletes {@link DailyTodo} from database source with {@param dailyTodoId}.
     *
     * @param dailyTodoId - integer value representing Id of {@link DailyTodo} to be deleted.
     */
    void deleteDailyTodo(int dailyTodoId);

}
