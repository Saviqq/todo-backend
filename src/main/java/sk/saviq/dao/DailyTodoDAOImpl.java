package sk.saviq.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import sk.saviq.exception.DailyTodoNotFoundException;
import sk.saviq.model.DailyTodo;
import sk.saviq.model.User;

import javax.persistence.TypedQuery;
import java.util.Date;
import java.util.List;

/**
 * Data access object implementation of {@link DailyTodoDAO} for {@link DailyTodo}.
 *
 * @author psavka
 */
@Repository
public class DailyTodoDAOImpl implements DailyTodoDAO {

    private SessionFactory sessionFactory;

    @Autowired
    public DailyTodoDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    /**
     * {@inheritDoc}
     */
    public DailyTodo getDailyTodoById(int dailyTodoId) {
        Session session = sessionFactory.getCurrentSession();

        DailyTodo dailyTodo = session.get(DailyTodo.class, dailyTodoId);
        if (dailyTodo == null) {
            throw new DailyTodoNotFoundException("DailyTodo with id {" + dailyTodoId +"} not found");
        }

        return dailyTodo;
    }

    /**
     * {@inheritDoc}
     */
    public List<DailyTodo> getAllDailyTodoOfUser(int userId) {
        Session session = sessionFactory.getCurrentSession();

        TypedQuery<DailyTodo> query = session.createQuery("select D from DailyTodo D where user_id=:userId", DailyTodo.class);
        query.setParameter("userId", userId);

        List<DailyTodo> dailyTodoList = query.getResultList();
        if(dailyTodoList == null) {
            throw new DailyTodoNotFoundException("List of DailyTodo for User with id {" + userId +"} not found");
        }

        return dailyTodoList;
    }

    /**
     * {@inheritDoc}
     */
    public List<DailyTodo> getAllDailyTodoOfUserForSpecificDay(int userId, Date date) {
        Session session = sessionFactory.getCurrentSession();

        TypedQuery<DailyTodo> query = session.createQuery("select D from DailyTodo D where user_id=:userId and date=:date", DailyTodo.class);
        query.setParameter("userId", userId);
        query.setParameter("date", date);

        List<DailyTodo> dailyTodoList = query.getResultList();
        if(dailyTodoList == null) {
            throw new DailyTodoNotFoundException("List of DailyTodo for User with id {" + userId +"} and date {" + date + "} not found");
        }

        return dailyTodoList;
    }

    /**
     * {@inheritDoc}
     */
    public void saveDailyTodoOfUser(DailyTodo dailyTodo, int userId) {
        Session session = sessionFactory.getCurrentSession();

        User user = session.get(User.class, userId);
        if(user == null) {
            throw new DailyTodoNotFoundException("User with id {" + userId +"} not found, so DailyTodo was not saved");
        }
        user.addDailyTodo(dailyTodo);

        session.save(user);
    }

    public void updateDailyTodo(DailyTodo dailyTodo) {
        Session session = sessionFactory.getCurrentSession();
        // TODO correct exception handling
        session.update(dailyTodo);
    }

    /**
     * {@inheritDoc}
     */
    public void deleteDailyTodo(int dailyTodoId) {
        Session session = sessionFactory.getCurrentSession();

        DailyTodo  dailyTodo = session.get(DailyTodo.class, dailyTodoId);
        if (dailyTodo == null) {
            throw new DailyTodoNotFoundException("DailyTodo with id {" + dailyTodoId +"} not found");
        }

        session.delete(dailyTodo);
    }

}
