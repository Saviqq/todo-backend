package sk.saviq.aspect;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
public class AOPExpressions {

    @Pointcut("execution(* sk.saviq.dao.*.*(..))")
    public void forDAOMethods() {}

    @Pointcut("execution(!void sk.saviq.dao.*.*(..))")
    public void forDAOMethodNotVoid() {}

    @Pointcut("execution(* sk.saviq.service.*.*(..))")
    public void forServiceMethods() {}

    @Pointcut("execution(!void sk.saviq.service.*.*(..))")
    public void forServiceMethodsNotVoid() {}
}

