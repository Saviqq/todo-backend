package sk.saviq.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

@Aspect
@Component
public class DAOAspects {

    private Logger logger = Logger.getLogger(getClass().getName());

    @Before("sk.saviq.aspect.AOPExpressions.forDAOMethods()")
    public void logBeforeDAOMethodCall(JoinPoint joinPoint) {
        logger.info("Method to be called: " + joinPoint.getSignature().toLongString());

        Object[] arguments = joinPoint.getArgs();
        for(Object argument : arguments) {
            logger.info("Passed argument to method: " + argument.getClass() + ": " + argument.toString());
        }
    }

    @AfterReturning(pointcut = "sk.saviq.aspect.AOPExpressions.forDAOMethodNotVoid()", returning = "result")
    public void logAfterReturningDAOMethodCall(JoinPoint joinPoint, Object result) {
        logger.info("Method called: " + joinPoint.getSignature().toLongString());
        logger.info("Method return result: " + result.getClass() + " " + result.toString());
    }
}
