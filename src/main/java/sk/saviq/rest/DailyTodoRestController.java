package sk.saviq.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;
import sk.saviq.model.DailyTodo;
import sk.saviq.service.DailyTodoService;

import java.util.Date;
import java.util.List;

import static org.springframework.format.annotation.DateTimeFormat.ISO.DATE;

@RestController
@RequestMapping("/api")
public class DailyTodoRestController {

    private DailyTodoService dailyTodoService;

    @Autowired
    public DailyTodoRestController(DailyTodoService dailyTodoService) {
        this.dailyTodoService = dailyTodoService;
    }

    @GetMapping("/dailyTodo/{dailyTodoId}")
    public DailyTodo getDailyTodoById(@PathVariable int dailyTodoId) {
        return dailyTodoService.getDailyTodoById(dailyTodoId);
    }


    @GetMapping("/dailyTodo/forUser/{userId}")
    public List<DailyTodo> getAllDailyTodoOfUser(@PathVariable int userId) {
        return dailyTodoService.getAllDailyTodoOfUser(userId);
    }

    @GetMapping("/dailyTodo/forToday/{userId}")
    public List<DailyTodo> getAllDailyTodoOfUserForCurrentDay(@PathVariable int userId) {
        return dailyTodoService.getAllDailyTodoOfUserForCurrentDay(userId);
    }

    @GetMapping("/dailyTodo/forDay/{userId}/{date}")
    public List<DailyTodo> getAllDailyTodoOfUserForSpecificDay(@PathVariable int userId, @PathVariable @DateTimeFormat(iso = DATE) Date date) {
        return dailyTodoService.getAllDailyTodoOfUserForSpecificDay(userId, date);
    }


    @PostMapping("/dailyTodo/{userId}")
    public DailyTodo saveDailyTodoOfUser(@RequestBody DailyTodo dailyTodo, @PathVariable int userId) {
        dailyTodoService.saveDailyTodoOfUser(dailyTodo, userId);

        return dailyTodo;
    }

    @PutMapping("/dailyTodo")
    public DailyTodo updateDailyTodo(@RequestBody DailyTodo dailyTodo) {
        dailyTodoService.updateDailyTodo(dailyTodo);

        return dailyTodo;
    }

    @DeleteMapping("/dailyTodo/{dailyTodoId}")
    public String deleteDailyTodoById(@PathVariable int dailyTodoId) {
        dailyTodoService.deleteDailyTodoById(dailyTodoId);

        return "Deleted DailyTodo with id: " + dailyTodoId;
    }

}
