package sk.saviq.exception;

import static java.lang.System.currentTimeMillis;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.NOT_FOUND;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class DailyTodoExceptionHandler {

    @ExceptionHandler
    public ResponseEntity<DailyTodoErrorResponse> handleException(DailyTodoNotFoundException exception) {
        DailyTodoErrorResponse error = new DailyTodoErrorResponse();
        error.setStatus(NOT_FOUND.value());
        error.setMessage(exception.getMessage());
        error.setTimeStamp(currentTimeMillis());

        return new ResponseEntity<DailyTodoErrorResponse>(error, NOT_FOUND);
    }

    @ExceptionHandler
    public ResponseEntity<DailyTodoErrorResponse> handleException(Exception exception) {
        DailyTodoErrorResponse error = new DailyTodoErrorResponse();
        error.setStatus(BAD_REQUEST.value());
        error.setMessage(exception.getMessage());
        error.setTimeStamp(currentTimeMillis());

        return new ResponseEntity<DailyTodoErrorResponse>(error, NOT_FOUND);
    }
}
