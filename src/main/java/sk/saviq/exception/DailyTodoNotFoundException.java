package sk.saviq.exception;

public class DailyTodoNotFoundException extends RuntimeException {

    public DailyTodoNotFoundException(String message) {
        super(message);
    }

}
