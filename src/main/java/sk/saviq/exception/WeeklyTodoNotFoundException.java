package sk.saviq.exception;

public class WeeklyTodoNotFoundException extends RuntimeException {

    public WeeklyTodoNotFoundException(String message) {
        super(message);
    }
}
