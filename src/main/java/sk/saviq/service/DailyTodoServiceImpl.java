package sk.saviq.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sk.saviq.dao.DailyTodoDAO;
import sk.saviq.model.DailyTodo;

import java.util.Date;
import java.util.List;

/**
 * Service implementation of {@link DailyTodoService} for {@link DailyTodo}
 *
 * @author psavka
 */
@Service
@Transactional("transactionManager")
public class DailyTodoServiceImpl implements DailyTodoService {

    private DailyTodoDAO dailyTodoDAO;

    @Autowired
    public DailyTodoServiceImpl(DailyTodoDAO dailyTodoDAO) {
        this.dailyTodoDAO = dailyTodoDAO;
    }

    /**
     * {@inheritDoc}
     */
    public DailyTodo getDailyTodoById(int dailyTodoId) {
        return dailyTodoDAO.getDailyTodoById(dailyTodoId);
    }

    /**
     * {@inheritDoc}
     */
    public List<DailyTodo> getAllDailyTodoOfUser(int userId) {
        return dailyTodoDAO.getAllDailyTodoOfUser(userId);
    }

    /**
     * {@inheritDoc}
     */
    public List<DailyTodo> getAllDailyTodoOfUserForCurrentDay(int userId) {
        return getAllDailyTodoOfUserForSpecificDay(userId, new Date());
    }

    /**
     * {@inheritDoc}
     */
    public List<DailyTodo> getAllDailyTodoOfUserForSpecificDay(int userId, Date date) {
        return dailyTodoDAO.getAllDailyTodoOfUserForSpecificDay(userId, date);
    }

    /**
     * {@inheritDoc}
     */
    public void saveDailyTodoOfUser(DailyTodo dailyTodo, int userId) {
        dailyTodoDAO.saveDailyTodoOfUser(dailyTodo, userId);
    }

    /**
     * {@inheritDoc}
     */
    public void updateDailyTodo(DailyTodo dailyTodo) {
        dailyTodoDAO.updateDailyTodo(dailyTodo);

    }

    /**
     * {@inheritDoc}
     */
    public void deleteDailyTodoById(int dailyTodoId) {
        dailyTodoDAO.deleteDailyTodo(dailyTodoId);
    }
}
