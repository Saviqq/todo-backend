package sk.saviq.service;

import sk.saviq.dao.DailyTodoDAO;
import sk.saviq.model.DailyTodo;
import sk.saviq.model.User;

import java.util.Date;
import java.util.List;

/**
 * Service interface for {@link DailyTodo}. It defines methods,
 * that forwards requests to access CRUD and other operations
 * on {@link DailyTodo} to {@link DailyTodoDAO}.
 * <p>
 * Methods are implemented by {@link DailyTodoServiceImpl}
 *
 * @author psavka
 */
public interface DailyTodoService {

    /**
     * Gets {@link DailyTodo} with {@param dailyTodoId} from database source
     * by forwarding the request to {@link DailyTodoDAO}.
     *
     * @param dailyTodoId - integer value representing Id of {@link DailyTodo}.
     * @return {@link DailyTodo}.
     */
    DailyTodo getDailyTodoById(int dailyTodoId);

    /**
     * Gets {@link List<DailyTodo>} of {@link User} with {@param userId} from
     * database source by forwarding the request to {@link DailyTodoDAO}.
     *
     * @param userId - integer value representing Id of {@link User}.
     * @return {@link List<DailyTodo>} - that belongs to the {@link User}
     */
    List<DailyTodo> getAllDailyTodoOfUser(int userId);

    /**
     * Gets {@link List<DailyTodo>} with date assigned to current day of {@link User}
     * with {@param userId} from database source by forwarding the request to {@link DailyTodoDAO}.
     *
     * @param userId - integer value representing Id of {@link User}.
     * @return {@link List<DailyTodo>} - with assigned date as current day that
     * belongs to the {@link User}.
     */
    List<DailyTodo> getAllDailyTodoOfUserForCurrentDay(int userId);

    /**
     * Gets {@link List<DailyTodo>} with date assigned to {@param date} of {@link User}
     * with {@param userId} from database source by forwarding the request to {@link DailyTodoDAO}.
     *
     * @param userId - integer value representing Id of {@link User}.
     * @param date - date value by which {@link List<DailyTodo>} of {@link User} are assigned.
     * @return {@link List<DailyTodo>} - that belongs to the {@link User} with specific day assigned.
     */
    List<DailyTodo> getAllDailyTodoOfUserForSpecificDay(int userId, Date date);

    /**
     * Saves new {@param dailyTodo} of {@link DailyTodo} for {@link User} with {@param userId} to
     * database source by forwarding the request to {@link DailyTodoDAO}.
     *
     * @param dailyTodo - {@link DailyTodo} to be saved
     * @param userId - integer value representing Id of {@link User}.
     */
    void saveDailyTodoOfUser(DailyTodo dailyTodo, int userId);


    /**
     * Updates {@param dailyTodo} of {@link DailyTodo} with new data in database source by
     * forwarding the request to {@link DailyTodoDAO}
     *
     * @param dailyTodo- {@link DailyTodo} to be updated
     */
    void updateDailyTodo(DailyTodo dailyTodo);

    /**
     * Deletes {@link DailyTodo} with {@param dailyTodoId} from database source by forwarding
     * the request to {@link DailyTodoDAO}.
     *
     * @param dailyTodoId - integer value representing Id of {@link DailyTodo} to be deleted.
     */
    void deleteDailyTodoById(int dailyTodoId);
}
