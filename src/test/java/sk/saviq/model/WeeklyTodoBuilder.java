package sk.saviq.model;

import java.util.Date;

import static org.springframework.test.util.ReflectionTestUtils.setField;

public class WeeklyTodoBuilder {

    private WeeklyTodo weeklyTodo;

    public WeeklyTodoBuilder() { weeklyTodo = new WeeklyTodo(); }

    public WeeklyTodoBuilder id(int id) {
        setField(weeklyTodo, "id", id);
        return this;
    }

    public WeeklyTodoBuilder todo(String todo) {
        setField(weeklyTodo, "todo", todo);
        return this;
    }

    public WeeklyTodoBuilder done(boolean done) {
        setField(weeklyTodo, "done", done);
        return this;
    }

    public WeeklyTodoBuilder date(Date date) {
        setField(weeklyTodo, "date", date);
        return this;
    }

    public WeeklyTodo build() {
        return weeklyTodo;
    }
}
