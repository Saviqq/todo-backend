package sk.saviq.model;

import org.junit.Before;
import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class DailyTodoTest {

    private DailyTodo dailyTodo;

    @Before
    public void setUp() {
        dailyTodo = new DailyTodo();
    }

    @Test
    public void test_idSetterAndGetter() {
        dailyTodo.setId(1);

        assertEquals(dailyTodo.getId(),1);
    }

    @Test
    public void test_todoSetterAndGetter() {
        dailyTodo.setTodo("test_todo");

        assertEquals(dailyTodo.getTodo(), "test_todo");
    }

    @Test
    public void test_doneSetterAndGetter() {
        dailyTodo.setDone(false);

        assertFalse(dailyTodo.isDone());
    }

    @Test
    public void test_dateSetterAndGetter() {
        Date date = new Date();
        dailyTodo.setDate(date);

        assertEquals(dailyTodo.getDate(), date);
    }

}