package sk.saviq.model;

import static org.springframework.test.util.ReflectionTestUtils.setField;

import java.util.Date;

public class DailyTodoBuilder {

    private DailyTodo dailyTodo;

    public DailyTodoBuilder() {
        dailyTodo = new DailyTodo();
    }

    public DailyTodoBuilder id(int id) {
        setField(dailyTodo, "id", id);
        return this;
    }

    public DailyTodoBuilder todo(String todo) {
        setField(dailyTodo, "todo", todo);
        return this;
    }

    public DailyTodoBuilder done(boolean done) {
        setField(dailyTodo, "done", done);
        return this;
    }

    public DailyTodoBuilder date(Date date) {
        setField(dailyTodo, "date", date);
        return this;
    }

    public DailyTodo build() {
        return dailyTodo;
    }

}
