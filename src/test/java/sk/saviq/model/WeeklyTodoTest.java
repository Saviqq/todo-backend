package sk.saviq.model;

import org.junit.Before;
import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class WeeklyTodoTest {

    private WeeklyTodo weeklyTodo;

    @Before
    public void setUp() {
        weeklyTodo = new WeeklyTodo();
    }

    @Test
    public void test_idSetterAndGetter() {
        weeklyTodo.setId(1);

        assertEquals(weeklyTodo.getId(), 1);
    }

    @Test
    public void test_todoSetterAndGetter() {
        weeklyTodo.setTodo("test_todo");

        assertEquals(weeklyTodo.getTodo(), "test_todo");
    }

    @Test
    public void test_doneSetterAndGetter() {
        weeklyTodo.setDone(false);

        assertFalse(weeklyTodo.isDone());
    }

    @Test
    public void test_dateSetterAndGetter() {
        Date date = new Date();
        weeklyTodo.setDate(date);

        assertEquals(weeklyTodo.getDate(), date);
    }

}