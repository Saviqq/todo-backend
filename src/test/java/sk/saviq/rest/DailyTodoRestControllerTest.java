package sk.saviq.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import sk.saviq.config.AppConfig;
import sk.saviq.model.DailyTodo;
import sk.saviq.service.DailyTodoService;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static sk.saviq.TestUtils.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = AppConfig.class)
@WebAppConfiguration
public class DailyTodoRestControllerTest {

    private MockMvc mockMvc;

    @Mock
    private DailyTodoService dailyTodoService;

    @InjectMocks
    private DailyTodoRestController dailyTodoRestController;

    @Before
    public void setUp() {
        initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(dailyTodoRestController).build();
    }

    @Test
    public void test_getDailyTodoById() throws Exception {
        Date date = getTestDateFromCalendar();
        DailyTodo dailyTodo = createDailyTodo();

        when(dailyTodoService.getDailyTodoById(1)).thenReturn(dailyTodo);

        mockMvc.perform(get("/api/dailyTodo/{dailyTodoId}", 1))
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.id", is(0)))
                .andExpect(jsonPath("$.todo", is("todo_test0")))
                .andExpect(jsonPath("$.done", is(false)))
                .andExpect(jsonPath("$.date", is(date.getTime())));

        verify(dailyTodoService, times(1)).getDailyTodoById(1);
        verifyNoMoreInteractions(dailyTodoService);
    }

    @Test
    public void getAllDailyTodoOfUser() throws Exception {
        Date date = getTestDateFromCalendar();
        List<DailyTodo> dailyTodoList = createTestDailyTodoList(2);

        when(dailyTodoService.getAllDailyTodoOfUser(1)).thenReturn(dailyTodoList);

        mockMvc.perform(get("/api/dailyTodo/forUser/{userId}", 1))
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$[0].id", is(0)))
                .andExpect(jsonPath("$[0].todo", is("todo_test0")))
                .andExpect(jsonPath("$[0].done", is(false)))
                .andExpect(jsonPath("$[0].date", is(date.getTime())))
                .andExpect(jsonPath("$[1].id", is(1)))
                .andExpect(jsonPath("$[1].todo", is("todo_test1")))
                .andExpect(jsonPath("$[1].done", is(false)))
                .andExpect(jsonPath("$[1].date", is(date.getTime())));

        verify(dailyTodoService, times(1)).getAllDailyTodoOfUser(1);
        verifyNoMoreInteractions(dailyTodoService);
    }

    @Test
    public void getAllDailyTodoOfUserForCurrentDay() throws Exception {
        Date date = getTestDateFromCalendar();
        List<DailyTodo> dailyTodoList = createTestDailyTodoList(2);

        when(dailyTodoService.getAllDailyTodoOfUserForCurrentDay(1)).thenReturn(dailyTodoList);

        mockMvc.perform(get("/api/dailyTodo/forToday/{userId}", 1))
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$[0].id", is(0)))
                .andExpect(jsonPath("$[0].todo", is("todo_test0")))
                .andExpect(jsonPath("$[0].done", is(false)))
                .andExpect(jsonPath("$[0].date", is(date.getTime())))
                .andExpect(jsonPath("$[1].id", is(1)))
                .andExpect(jsonPath("$[1].todo", is("todo_test1")))
                .andExpect(jsonPath("$[1].done", is(false)))
                .andExpect(jsonPath("$[1].date", is(date.getTime())));

        verify(dailyTodoService, times(1)).getAllDailyTodoOfUserForCurrentDay(1);
        verifyNoMoreInteractions(dailyTodoService);
    }

    @Test
    public void getAllDailyTodoOfUserForSpecificDay() throws Exception {
        Date date = getTestDateFromCalendar();
        DateFormat df = new SimpleDateFormat("YYYY-MM-dd");
        String dateToString = df.format(date);
        List<DailyTodo> dailyTodoList = createTestDailyTodoList(2);

        when(dailyTodoService.getAllDailyTodoOfUserForSpecificDay(1, date)).thenReturn(dailyTodoList);

        mockMvc.perform(get("/api/dailyTodo/forDay/{userId}/{date}", 1, dateToString))
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$[0].id", is(0)))
                .andExpect(jsonPath("$[0].todo", is("todo_test0")))
                .andExpect(jsonPath("$[0].done", is(false)))
                .andExpect(jsonPath("$[0].date", is(date.getTime())))
                .andExpect(jsonPath("$[1].id", is(1)))
                .andExpect(jsonPath("$[1].todo", is("todo_test1")))
                .andExpect(jsonPath("$[1].done", is(false)))
                .andExpect(jsonPath("$[1].date", is(date.getTime())));

        verify(dailyTodoService, times(1)).getAllDailyTodoOfUserForSpecificDay(1, date);
        verifyNoMoreInteractions(dailyTodoService);
    }

    @Test
    public void saveDailyTodoOfUser() throws Exception {
        Date date = getTestDateFromCalendar();
        DailyTodo dailyTodo = createDailyTodoToSave();

        ObjectMapper objectMapper = new ObjectMapper();

        String json = objectMapper.writeValueAsString(dailyTodo);

        mockMvc.perform(post("/api/dailyTodo/{userId}", 1).contentType(APPLICATION_JSON_UTF8).content(json))
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.todo", is("todo_test")))
                .andExpect(jsonPath("$.done", is(false)))
                .andExpect(jsonPath("$.date", is(date.getTime())));
    }

    @Test
    public void updateDailyTodo() throws Exception {
        Date date = getTestDateFromCalendar();
        DailyTodo dailyTodo = createDailyTodoToUpdate();

        ObjectMapper objectMapper = new ObjectMapper();

        String json = objectMapper.writeValueAsString(dailyTodo);

        mockMvc.perform(put("/api/dailyTodo").contentType(APPLICATION_JSON_UTF8).content(json))
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.id", is(10)))
                .andExpect(jsonPath("$.todo", is("todo_test")))
                .andExpect(jsonPath("$.done", is(false)))
                .andExpect(jsonPath("$.date", is(date.getTime())));
    }

    @Test
    public void deleteDailyTodoById() throws Exception {
        int dailyTodoId = 1;

        mockMvc.perform(delete("/api/dailyTodo/{dailyTodoId}", dailyTodoId))
                .andExpect(status().isOk())
                .andExpect(content().contentType(TEXT_PLAIN_ISO))
                .andExpect(content().string("Deleted DailyTodo with id: " + dailyTodoId));
    }

}