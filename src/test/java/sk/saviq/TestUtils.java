package sk.saviq;

import org.springframework.http.MediaType;
import sk.saviq.model.DailyTodo;
import sk.saviq.model.DailyTodoBuilder;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.http.MediaType.TEXT_PLAIN;

public class TestUtils {

    public static final MediaType APPLICATION_JSON_UTF8 =
            new MediaType(APPLICATION_JSON.getType(), APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

    public static final MediaType TEXT_PLAIN_ISO =
            new MediaType(TEXT_PLAIN.getType(), TEXT_PLAIN.getSubtype(), Charset.forName("ISO-8859-1"));

    public static Date getTestDateFromCalendar() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(1531526400000L);

        return calendar.getTime();
    }

    public static List<DailyTodo> createTestDailyTodoList(int lengthOfList) {
        Date date = getTestDateFromCalendar();
        List<DailyTodo> dailyTodoList = new ArrayList<DailyTodo>();

        for (int i = 0; i < lengthOfList; i++) {
            DailyTodo dailyTodo = new DailyTodoBuilder()
                    .id(i)
                    .todo("todo_test" + i)
                    .done(false)
                    .date(date)
                    .build();
            dailyTodoList.add(dailyTodo);
        }

        return dailyTodoList;
    }

    public static DailyTodo createDailyTodo() {
        Date date = getTestDateFromCalendar();
        return new DailyTodoBuilder()
                .id(0)
                .todo("todo_test" + 0)
                .done(false)
                .date(date)
                .build();
    }

    public static DailyTodo createDailyTodoToSave() {
        Date date = getTestDateFromCalendar();
        return new DailyTodoBuilder()
                .todo("todo_test")
                .done(false)
                .date(date)
                .build();
    }

    public static DailyTodo createDailyTodoToUpdate() {
        Date date = getTestDateFromCalendar();
        return new DailyTodoBuilder()
                .id(10)
                .todo("todo_test")
                .done(false)
                .date(date)
                .build();
    }

}
