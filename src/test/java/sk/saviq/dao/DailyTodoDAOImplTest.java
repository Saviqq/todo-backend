package sk.saviq.dao;

import org.hibernate.SessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import sk.saviq.TestConfig;
import sk.saviq.model.DailyTodo;
import sk.saviq.model.User;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static sk.saviq.TestUtils.getTestDateFromCalendar;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = TestConfig.class)
@Transactional
public class DailyTodoDAOImplTest {

    @Autowired
    private DailyTodoDAO dailyTodoDAO;

    @Autowired
    SessionFactory sessionFactory;

    @Test
    public void test_getDailyTodoById() {
        int dailyTodoId = createDailyTodo();

        DailyTodo dailyTodo = dailyTodoDAO.getDailyTodoById(dailyTodoId);

        assertEquals(dailyTodo.getId(), dailyTodoId);
    }

    @Test
    public void test_getAllDailyTodoForUser() {
        int userId = createUserWithDailyTodoList();

        List<DailyTodo> dailyTodoList = dailyTodoDAO.getAllDailyTodoOfUser(userId);

        assertEquals(dailyTodoList.size(), 2);
        assertEquals(dailyTodoList.get(0).getTodo(), "dailyTodo_test 01");
        assertEquals(dailyTodoList.get(1).getTodo(), "dailyTodo_test 02");
    }

    @Test
    public void test_getAllDailyTodoOfUserForSpecificDay() {
        int userId = createUserWithDailyTodoOfSpecificDay();
        Date date = getTestDateFromCalendar();

        List<DailyTodo> dailyTodoList = dailyTodoDAO.getAllDailyTodoOfUserForSpecificDay(userId, date);

        assertEquals(dailyTodoList.size(), 1);
        assertEquals(dailyTodoList.get(0).getTodo(), "dailyTodo_test");
    }

    @Test
    public void test_saveDailyTodoOfUser() {
        int userId = createUserWithDailyTodoList();
        int dailyTodoTableRowsCount = getDailyTodoTableRowsCount();

        User user = getUserById(userId);
        DailyTodo dailyTodo = new DailyTodo("saveDailyTodoOfUser", false, new Date());
        user.addDailyTodo(dailyTodo);

        dailyTodoDAO.saveDailyTodoOfUser(dailyTodo, userId);

        assertEquals(dailyTodoTableRowsCount + 1, getDailyTodoTableRowsCount());
    }

    @Test
    public void test_updateDailyTodo() {
        int dailyTodoId = createDailyTodo();
        int dailyTodoTableRowsCount = getDailyTodoTableRowsCount();
        DailyTodo notUpdatedDailyTodo = dailyTodoDAO.getDailyTodoById(dailyTodoId);

        assertEquals(notUpdatedDailyTodo.getId(), dailyTodoId);

        notUpdatedDailyTodo.setTodo("updateDailyTodo");
        dailyTodoDAO.updateDailyTodo(notUpdatedDailyTodo);

        DailyTodo updatedDailyTodo = dailyTodoDAO.getDailyTodoById(dailyTodoId);

        assertEquals(dailyTodoTableRowsCount, getDailyTodoTableRowsCount());
        assertEquals(updatedDailyTodo.getTodo(), notUpdatedDailyTodo.getTodo());
    }

    @Test
    public void test_deleteDailyTodo() {
        int dailyTodoId = createDailyTodo();

        int dailyTodoTableRowsCount = getDailyTodoTableRowsCount();

        dailyTodoDAO.deleteDailyTodo(dailyTodoId);

        sessionFactory.getCurrentSession().flush();
        assertEquals(dailyTodoTableRowsCount - 1, getDailyTodoTableRowsCount());
    }

    private int createDailyTodo() {
        DailyTodo dailyTodo = new DailyTodo("dailyTodo_test", false, new Date());

        sessionFactory.getCurrentSession().save(dailyTodo);

        return dailyTodo.getId();
    }

    private int createUserWithDailyTodoList() {
        User user = new User("Joe", "Doe", "joedoe@gmail.com");
        user.addDailyTodo(new DailyTodo("dailyTodo_test 01", false, new Date()));
        user.addDailyTodo(new DailyTodo("dailyTodo_test 02", false, new Date()));

        sessionFactory.getCurrentSession().save(user);

        return user.getId();
    }

    private int createUserWithDailyTodoOfSpecificDay() {
        Date date = getTestDateFromCalendar();

        User user = new User("Joe", "Doe", "joedoe@gmail.com");
        user.addDailyTodo(new DailyTodo("dailyTodo_test", false, date));

        sessionFactory.getCurrentSession().save(user);

        return user.getId();
    }

    private int getDailyTodoTableRowsCount() {
        return ((Long) sessionFactory.getCurrentSession().createQuery("select count(*) from DailyTodo").uniqueResult()).intValue();
    }

    private User getUserById(int userId) {
        return sessionFactory.getCurrentSession().get(User.class, userId);
    }

}