package sk.saviq.dao;

import org.hibernate.SessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import sk.saviq.TestConfig;
import sk.saviq.model.User;
import sk.saviq.model.WeeklyTodo;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static sk.saviq.TestUtils.getTestDateFromCalendar;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = TestConfig.class)
@Transactional
public class WeeklyTodoDAOImplTest {

    @Autowired
    private WeeklyTodoDAO weeklyTodoDAO;

    @Autowired
    SessionFactory sessionFactory;

    @Test
    public void test_getWeeklyTodoById() {
        int weeklyTodoId = createWeeklyTodo();

        WeeklyTodo weeklyTodo = weeklyTodoDAO.getWeeklyTodoById(weeklyTodoId);

        assertEquals(weeklyTodo.getId(), weeklyTodoId);
    }

    @Test
    public void test_getAllWeeklyTodoOfUser() {
        int userId = createUserWithWeeklyTodoList();

        List<WeeklyTodo> weeklyTodoList = weeklyTodoDAO.getAllWeeklyTodoOfUser(userId);

        assertEquals(weeklyTodoList.size(), 2);
        assertEquals(weeklyTodoList.get(0).getTodo(), "weeklyTodo_test 01");
        assertEquals(weeklyTodoList.get(1).getTodo(), "weeklyTodo_test 02");
    }

    @Test
    public void test_getAllWeeklyTodoOfUserForSpecificday() {
        int userId = createUserWithWeeklyTodoOfSpecificDay();
        Date date = getTestDateFromCalendar();

        List<WeeklyTodo> weeklyTodoList = weeklyTodoDAO.getAllWeeklyTodoOfUserForSpecificday(userId, date);

        assertEquals(weeklyTodoList.size(), 1);
        assertEquals(weeklyTodoList.get(0).getTodo(), "weeklyTodo_test");
    }

    @Test
    public void test_saveWeeklyTodoOfUser() {
        int userId = createUserWithWeeklyTodoList();
        int weeklyTodoTableRowsCount = getWeeklyTodoTableRowsCount();

        User user = getUserById(userId);
        WeeklyTodo weeklyTodo = new WeeklyTodo("saveWeeklyTodoOfUser", false, new Date());
        user.addWeeklyTodo(weeklyTodo);

        weeklyTodoDAO.saveWeeklyTodoOfUser(weeklyTodo, userId);

        assertEquals(weeklyTodoTableRowsCount + 1, getWeeklyTodoTableRowsCount());
    }

    @Test
    public void test_updateWeeklyTodo() {
        int weeklyTodoId = createWeeklyTodo();
        int weeklyTodoTableRowsCount = getWeeklyTodoTableRowsCount();
        WeeklyTodo notUpdatedWeeklyTodo = weeklyTodoDAO.getWeeklyTodoById(weeklyTodoId);

        assertEquals(notUpdatedWeeklyTodo.getId(), weeklyTodoId);

        notUpdatedWeeklyTodo.setTodo("updateWeeklyTodo");
        weeklyTodoDAO.updateWeeklyTodo(notUpdatedWeeklyTodo);

        WeeklyTodo updatedWeeklyTodo = weeklyTodoDAO.getWeeklyTodoById(weeklyTodoId);

        assertEquals(weeklyTodoTableRowsCount, getWeeklyTodoTableRowsCount());
        assertEquals(updatedWeeklyTodo.getTodo(), notUpdatedWeeklyTodo.getTodo());
    }

    @Test
    public void test_deleteWeeklyTodo() {
        int weeklyTodoId = createWeeklyTodo();

        int weeklyTodoTableRowsCount = getWeeklyTodoTableRowsCount();

        weeklyTodoDAO.deleteWeeklyTodo(weeklyTodoId);

        sessionFactory.getCurrentSession().flush();
        assertEquals(weeklyTodoTableRowsCount - 1, getWeeklyTodoTableRowsCount());
    }

    private int createWeeklyTodo() {
        WeeklyTodo weeklyTodo = new WeeklyTodo("weeklyTodo_test", false, new Date());

        sessionFactory.getCurrentSession().save(weeklyTodo);

        return weeklyTodo.getId();
    }

    private int createUserWithWeeklyTodoList() {
        User user = new User("Joe", "Doe", "joedoe@gmail.com");
        user.addWeeklyTodo(new WeeklyTodo("weeklyTodo_test 01", false, new Date()));
        user.addWeeklyTodo(new WeeklyTodo("weeklyTodo_test 02", false, new Date()));

        sessionFactory.getCurrentSession().save(user);

        return user.getId();
    }

    private int createUserWithWeeklyTodoOfSpecificDay() {
        Date date = getTestDateFromCalendar();

        User user = new User("Joe", "Doe", "joedoe@gmail.com");
        user.addWeeklyTodo(new WeeklyTodo("weeklyTodo_test", false, date));

        sessionFactory.getCurrentSession().save(user);

        return user.getId();
    }

    private int getWeeklyTodoTableRowsCount() {
        return ((Long) sessionFactory.getCurrentSession().createQuery("select count(*) from WeeklyTodo").uniqueResult()).intValue();
    }

    private User getUserById(int userId) {
        return sessionFactory.getCurrentSession().get(User.class, userId);
    }
}