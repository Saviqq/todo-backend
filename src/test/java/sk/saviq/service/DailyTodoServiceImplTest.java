package sk.saviq.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.annotation.Transactional;
import sk.saviq.TestConfig;
import sk.saviq.dao.DailyTodoDAO;
import sk.saviq.model.DailyTodo;

import java.util.Date;

import static org.mockito.Matchers.*;
import static org.mockito.Mockito.verify;

/**
 * Class intended to test methods of service {@link DailyTodoServiceImpl}
 */
@RunWith(MockitoJUnitRunner.class)
@ContextConfiguration(classes = TestConfig.class)
@Transactional
public class DailyTodoServiceImplTest {

    @InjectMocks
    private DailyTodoServiceImpl dailyTodoService;

    @Mock
    private DailyTodoDAO dailyTodoDAO;

    @Test
    public void getDailyTodoOfUser_test() {
        dailyTodoService.getAllDailyTodoOfUser(any(Integer.class));

        verify(dailyTodoDAO).getAllDailyTodoOfUser(any(Integer.class));
    }

    @Test
    public void getAllDailyTodoOfUserForCurrentDay_test() {
        dailyTodoService.getAllDailyTodoOfUserForCurrentDay(eq(anyInt()));

        verify(dailyTodoDAO).getAllDailyTodoOfUserForSpecificDay(any(Integer.class), any(Date.class));
    }

    @Test
    public void getDailyTodoOfUserForSpecificDay_test() {
        dailyTodoService.getAllDailyTodoOfUserForSpecificDay(any(Integer.class), any(Date.class));

        verify(dailyTodoDAO).getAllDailyTodoOfUserForSpecificDay(any(Integer.class), any(Date.class));
    }

    @Test
    public void getDailyTodoById_test() {
        dailyTodoService.getDailyTodoById(any(Integer.class));

        verify(dailyTodoDAO).getDailyTodoById(any(Integer.class));
    }

    @Test
    public void saveDailyTodoOfUser_test() {
        dailyTodoService.saveDailyTodoOfUser(any(DailyTodo.class), any(Integer.class));

        verify(dailyTodoDAO).saveDailyTodoOfUser(any(DailyTodo.class),  any(Integer.class));
    }

    public void updateDailyTodo_test() {
        dailyTodoService.updateDailyTodo(any(DailyTodo.class));

        verify(dailyTodoDAO).updateDailyTodo(any(DailyTodo.class));
    }

    @Test
    public void deleteTodo_test() {
        dailyTodoService.deleteDailyTodoById(any(Integer.class));

        verify(dailyTodoDAO).deleteDailyTodo(any(Integer.class));
    }

}